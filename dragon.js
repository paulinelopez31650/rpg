import {Enemy} from './enemy.js';

export class Dragon extends Enemy {
   
    // Ses attributs :
    race = "Dragon";
    //statutEnVol = false;
    statutEnVol=false;
    // Son constructor :
    constructor (name){
        super (name);
    }

    // Ses méthodes :

    attack(){
        let attaque;
        if (!this.statutEnVol){
        let grigri = Math.floor(Math.random(1,100)*100);
        console.log(grigri);
        
        if (grigri<=50){
            attaque= this.frappe();
            this.fly();
        }else{
            attaque=this.frappe();
        }
    }else{
        attaque=this.frappe();
    }
    return attaque;
    }
    frappe(){
        let frappe;
        if (this.statutEnVol){
            frappe= this.attackFromTheSky();
        }else {
            frappe= this.hitStrength;
        }
        return frappe;
    }

    fly(){
        this.statutEnVol= true;
        console.log(this.name+ ' s\'envole.');
    }
    attackFromTheSky(){
        console.log(this.name+' attaque depuis le ciel');
        let attaqueCiel = this.hitStrength*1.1;
        this.statutEnVol = false;
        console.log(this.name +' se pose.');
        return attaqueCiel;

    }
    getHitStrength(){}

      // Evolution PV resistance de 50% supplementaire
    setHealth(ajoutPV){
        if (ajoutPV<0){
            if(this.statutEnVol){
            this.health += ajoutPV/2*1.1;
            }else{
                this.health += ajoutPV/2;
            }
        }else{
            this.health += ajoutPV;
        }
        //tester si le personnage est encore en vie !
        if(this.health<=0){
        this.die();
        }
        return ('Les points de vie de ' + this.name + ' sont désormais de ')+ this.health + ('.');
    }
}


/*

Une methode fly et attackFromSky augmentent de 10% la resistance et 10% l'attaque. 
Leur attaques devront suivre la logique suivante:
attaque au sol
fly
attackFromSky
*/