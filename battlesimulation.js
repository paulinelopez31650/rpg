import {Battle} from "./battle.js";
import {Berseker} from "./berseker.js";
import {Assassin} from "./assassin.js";
import {Dragon} from "./dragon.js";
import {Werewolf} from "./werewolf.js";
import {Griffin} from "./griffin.js";
import {Golem} from "./golem.js";


//1 héros jusqu'à la mort
export class BattleSimulationUD extends Battle{


// Son constructor qui appelle les participants :
    constructor(combattantHero, combattantVilain){
    super(combattantHero,combattantVilain)
}


    // Ses méthodes :

    //Lancer le combat :
    laPartie(){
        this.letsGo()
        
        //tant que les combattants sont en forme, on lance des tours de jeu :
        this.tours();
        
     
    }

    tours(){
    while (this.combattantHero.health > 0 && this.combattantVilain.health > 0){
            
        this.tourDeJeu();
    } 
    
    if(this.combattantVilain.health === 0){
        // Si le vilain est mort, il est perdant + la manche est terminée on relance
        this.gagnant=this.combattantHero;
        this.perdant=this.combattantVilain;
        this.finDeManche();
        
    }else if(this.combattantHero.health === 0){
        // Si le héros est mort, c'est le perdant + la partie est terminée !
        this.gagnant=this.combattantVilain;
        this.perdant=this.combattantHero;
        this.finDePartie();
    }
}

    // partie initialisée, désormais en cours
    letsGo(){
    console.log('Un nouveau combat commence, '+this.combattantHero.name+' affronte '+this.combattantVilain.name+' :');
    this.statutPartie = true;
    }

    tourDeJeu(){
        // Qui frappe en premier ? Faire un random entre combattantHero et combattantVilain
        let lesCombattants=[this.combattantHero.name, this.combattantVilain.name]; 
        let quiCommence= Math.floor(Math.random()*2);

        // Si le random l'a choisi, c'est le hero qui frappe en premier !
        if(lesCombattants[quiCommence]===this.combattantHero.name){
            
            // Mettre la puissance d'attaque dans la variable frappeHéro et lancer l'attaque sur le Vilain
            let frappeHero=this.combattantHero.attack(this.combattantVilain);
            console.log(this.combattantHero.name + (" attaque ") + this.combattantVilain.name + (" avec une puissance de ") + frappeHero );
            //Appliquer les dégats sur les PV du Vilain
            this.combattantVilain.setHealth(-frappeHero);
            this.combattantVilain.getHealth();
         }

        // Sinon c'est le méchant qui frappe !
        else{

            //on compte les attaques du vilain pour si c'est un assassin
            this.compteurRound +=1;
            //console.log(this.compteurRound);
            //si c'est un assassin, on stocke le nbr d'attaque dans son attribut compteTour :
            if (this.combattantVilain.compteTour){
                this.combattantVilain.compteurTour=this.compteurRound;
            }
             // Mettre la puissance d'attaque dans la variable frappeVilain et lancer l'attaque sur le Hero
             let frappeVilain= this.combattantVilain.attack(this.combattantHero);
             //console.log(frappeVilain);
             console.log(this.combattantVilain.name + (" attaque ") + this.combattantHero.name + (" avec une puissance de ") + frappeVilain );
             //Appliquer les dégats sur les PV du Héro
             this.combattantHero.setHealth(-frappeVilain);
             this.combattantHero.getHealth();
        }        


    }

    finDePartie(){

        //partie finie :
        console.log("La partie est terminée !");
        console.log(this.gagnant.name + " a gagné !");

        //compter les points
        this.gagnant.setXp(3);
        this.gagnant.getXp();
        this.perdant.setXp(1);
        this.perdant.getXp();
    }
    finDeManche(){

        //partie finie :
        console.log("La manche est terminée !");
        console.log(this.gagnant.name + " a gagné !");
        
        //ajouter de la vie bonus au héros
        this.gagnant.setHealth(40);
        //compter les points
        // Ajouter 3XP au gagnant :
        this.gagnant.setXp(3);
        this.gagnant.getXp();
        // Ajouter 1XP au perdant "bravo d'avoir participé":
        this.perdant.setXp(1);
        this.perdant.getXp();
        console.log(this.combattantHero.name + ' a encore de l\'énergie et se prépare à affronter un nouvel adversaire')
        this.chgmtVilain();
        this.letsGo();
        this.tours();

    }
    chgmtVilain(){
        //random
        let grigri = Math.floor(Math.random(1,6)*6);
        let newVilain;
        if (grigri==0){
            newVilain = new Golem('Golem de Fer');
        }else if (grigri==1){
            newVilain = new Griffin('Griffon');
        }else if (grigri==2){
            newVilain = new Dragon('Flameche');
        }else if (grigri==3){
            newVilain = new Assassin('Lame');
        }else if (grigri==4){
            newVilain = new Berseker('Roublar');
        }else{
            newVilain = new Werewolf('Garou');
        }
        this.combattantVilain= newVilain;
    }
}

