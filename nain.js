import {Hero} from "./hero.js";

export class Nain extends Hero {

    // Ses attributs :
    race = "Nain";

    // Son constructor :
    constructor(name){
        super(name);
   }

   // Ses méthodes

     // Evolution PV 20% de chance de recevoir 50% de degats en moins
  setHealth(ajoutPV){
      // random pour le pourcentage de chance d'etre veinard :
      let grigri = Math.floor(Math.random(1,100)*100);

    if (ajoutPV<0 && grigri<=20){
        console.log(this.name +' a de la veine ! Il ne subit que 50% des dégats...')
        this.health += ajoutPV/2;
    }else{
        this.health +=ajoutPV;
    }
    //tester si le personnage est encore en vie !
    if(this.health<=0){
    this.die();
    }
    return ('Les points de vie de ' + this.name + ' sont désormais de ')+ this.health + ('.');
  }
}


