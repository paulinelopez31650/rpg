//classe Personnage (les autres classes en découleront)
export class Personnage {
  
  // Ses attributs :
  name;
  health  = 100;
  hitStrength  = 30;
  lvl = 1;
  xp = 0;
  
  // pour définir les prochains personnages en entrant leur nom en paramètre :
  constructor(name){
    this.name = name;
  }
  
  // On liste les méthodes:

  // Consulter son identité
  getName(){            
    return ('Son nom est... ')+ this.name +(' !');
  }
  
  // Changer d'identité
  setName(renomme) {
    this.name = renomme;
    return ('Son nom est désormais... ')+ this.name +(' !');
  }

  // Consulter son nombre de PV
  getHealth(){
    console.log('Les points de vie de '+this.name+' sont de '+this.health);
    return this.health;
  }
  
  // Evolution PV
  setHealth(ajoutPV){
    this.health += ajoutPV;
    //tester si le personnage est encore en vie !
    if(this.health<=0){
    this.die();
    }
    return ('Les points de vie de ' + this.name + ' sont désormais de ')+ this.health + ('.');
  }

  // Attaque d'une force de
  attack(){
    return this.hitStrength;
  }

  // La mort = santé à 0
  die(){
    this.health=0;
    console.log(this.name +' a trouvé la mort ! Game Over !');

  }

  //Consulter son niveau 
  getLvl(){
    return ('Le niveau de ')+ this.name + (' est de ')+ this.lvl + ('.');
  }

  //Evolution du niveau
  setLvl(ajoutLvl){
    this.lvl += ajoutLvl;
    return ('Le niveau de ') + this.name +(' est désormais de ')+ this.lvl + ('.');
  }

  //Consulter son expérience
  getXp(){
   return ('Les points d\'expérience de ')+ this.name +(' sont de ')+ this.xp + ('.');
  }

  // Evolution des XP
  setXp(ajoutXP){
    this.xp += ajoutXP;
    
    //tester si les XP sont arrivées à 10, si oui, augmenter d'un niveau :
    if (this.xp >= 10){
      console.log(this.name + ' augmente d\'un niveau !');
      this.xp -= 10;
      this.setLvl(1);
    }
    return ('Les points d\'expérience de ')+ this.name +(' sont désormais de ')+ this.xp + ('.');
  }

  // Consulter sa force de frappe
  getHitStrength(){
    return ('Sa force de frappe est de ')+ this.hitStrength +('.');
  }

  // Changer sa puissance de frappe
  setHitStrength(newPuissanceFrappe){
    this.hitStrength = newPuissanceFrappe;
    return ('Sa force de frappe est désormais de ')+ this.hitStrength +('.');
  }

  }

// Exemple de création de personnage et d'utilisation de ses méthodes

let laPoule = new Personnage("Pauline");// Créer une nouvelle classe
console.log(laPoule.getName()); // Demander son identité
laPoule.setName("La cocotte"); // Changer son identité
console.log(laPoule.getName()); // Redemander son identité