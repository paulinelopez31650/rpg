import {Hero} from "./hero.js";

export class Humain extends Hero {

    // Ses attributs :
    race= "Humain";
    
    // Son constructor :
    constructor(name){
        super(name);
   }
   // Ses méthodes
   
   //retourne sa force d'attaque avec
   //+10% de hitStrength sur les ennemis terrestres, -10% de hitStrength sur les ennemis volants
   attack(adversaire){
        if(adversaire.statutEnVol){
            console.log(this.name +" dit : \" Pas de chance, " + adversaire.name + " est volant, c'est râlant, mon attaque est moins forte !\"");
            return this.hitStrength*0.9;
        }else{
            console.log(this.name +" dit : \" La chance me sourit, " + adversaire.name + " est terrestre, je frappe plus fort, héhé !\"");
            return this.hitStrength*1.1;
    }
}

}
