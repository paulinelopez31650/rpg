import {Enemy} from './enemy.js';

export class Werewolf extends Enemy {
   
    // Ses attributs :
    race = "Werewolf";
    
    // Son constructor :
    constructor (name){
        super (name);
    }

    // Ses méthodes :

    // Evolution PV resistance de 50% supplementaire
    setHealth(ajoutPV){
        if (ajoutPV<0){
            this.health += ajoutPV/2;
        }else{
            this.health += ajoutPV;
        }
        //tester si le personnage est encore en vie !
        if(this.health<=0){
        this.die();
        }
        return ('Les points de vie de ' + this.name + ' sont désormais de ')+ this.health + ('.');
    }
    
}