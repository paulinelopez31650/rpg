import {Elf} from "./elf.js";
import {Nain} from "./nain.js";
import {Humain} from "./humain.js";
import {Berseker} from "./berseker.js";
import {Assassin} from "./assassin.js";
import {Dragon} from "./dragon.js";
import {Werewolf} from "./werewolf.js";
import {Griffin} from "./griffin.js";
import {Golem} from "./golem.js";
import {Battle} from "./battle.js";
import {BattleSimulationUD} from "./battlesimulation.js";
import {BattleSimulationVV} from "./battlesimulation2.js";
import {choisirGentil,choisirMechant,choisirGentil2,choisirMechant1,choisirMechant2, choixGentil, choixMechant, choixGentil1, choixMechant1, choixMechant2} from "./fonctions.js";
import { Personnage } from "./personnage.js";

document.getElementById("mechant-selecteur").addEventListener('change', choisirMechant);
document.getElementById("gentil-selecteur").addEventListener('change', choisirGentil);
document.getElementById("gentil-select").addEventListener('change', choisirGentil2);
document.getElementById("mechant-select1").addEventListener('change', choisirMechant1);
document.getElementById("mechant-select2").addEventListener('change', choisirMechant2);



function lanceur(){

    let pseudoGentil = document.getElementById('pseudo-gentil').value;
    let pseudoMechant = document.getElementById('pseudo-mechant').value;
    let newHero;
    let newEnnemy;
    switch (choixGentil){
        case "humain":
            newHero = new Humain(pseudoGentil);
            break;
        case "nain":
            newHero = new Nain(pseudoGentil);
            break;
        case "elf":
            newHero = new Elf(pseudoGentil);
            break;
    }  
    switch (choixMechant){
        case "assassin":
            newEnnemy = new Assassin(pseudoMechant);
            break;
        case "berseker":
            newEnnemy = new Berseker(pseudoMechant);
            break;
        case "dragon":
            newEnnemy = new Dragon(pseudoMechant);
            break;
        case "griffin":
            newEnnemy = new Griffin(pseudoMechant);
            break;
        case "golem":
            newEnnemy = new Golem(pseudoMechant);
            break;
        case "werewolf":
            newEnnemy = new Werewolf(pseudoMechant);
            break;
    } 
    console.log(newHero);
    console.log(newEnnemy);
    console.log(newHero.name +' affronte '+ newEnnemy.name);
    let newBattle = new Battle(newHero, newEnnemy);
    newBattle.laPartie();
}

document.getElementById("bouton1").onclick = lanceur;


//battle simulation héros jusqu'à la mort
function lanceur2(){

    let pseudoGentil = document.getElementById('pseudo-heros').value;
    let newHero;
    let newEnnemy;
    
    switch (choixGentil1){
        case "humain":
            newHero = new Humain(pseudoGentil);
            break;
        case "nain":
            newHero = new Nain(pseudoGentil);
            break;
        case "elf":
            newHero = new Elf(pseudoGentil);
            break;
    }  
    let grigri = Math.floor(Math.random(1,6)*6);
    console.log('LE NUM EST '+grigri)
        if ( grigri==0){
            newEnnemy = new Golem('Golem de Fer');
        }else if (grigri==1){
            newEnnemy = new Griffin('Griffon');
        }else if (grigri==2){
            newEnnemy = new Dragon('Flameche');
        }else if (grigri==3){
            newEnnemy = new Assassin('Lame');
        }else if (grigri==4){
            newEnnemy = new Berseker('Roublar');
        }else{
            newEnnemy = new Werewolf('Garou');
        }
    console.log(newHero);
    console.log(newHero.name +' affronte '+ newEnnemy.name);

    let newBattleUD = new BattleSimulationUD(newHero, newEnnemy);
    newBattleUD.laPartie();
}
document.getElementById("bouton2").onclick = lanceur2;

function lanceur3(){

    let pseudoMechant1 = document.getElementById('pseudo-mechant1').value;
    let pseudoMechant2 = document.getElementById('pseudo-mechant2').value;
    let newEnnemy1;
    let newEnnemy2;
    switch (choixMechant1){
        case "assassin":
            newEnnemy1 = new Assassin(pseudoMechant1);
            break;
        case "berseker":
            newEnnemy1 = new Berseker(pseudoMechant1);
            break;
        case "dragon":
            newEnnemy1 = new Dragon(pseudoMechant1);
            break;
        case "griffin":
            newEnnemy1 = new Griffin(pseudoMechant1);
            break;
        case "golem":
            newEnnemy1 = new Golem(pseudoMechant1);
            break;
        case "werewolf":
            newEnnemy1 = new Werewolf(pseudoMechant1);
            break;
    }  
    switch (choixMechant2){
        case "assassin":
            newEnnemy2 = new Assassin(pseudoMechant2);
            break;
        case "berseker":
            newEnnemy2 = new Berseker(pseudoMechant2);
            break;
        case "dragon":
            newEnnemy2 = new Dragon(pseudoMechant2);
            break;
        case "griffin":
            newEnnemy2 = new Griffin(pseudoMechant2);
            break;
        case "golem":
            newEnnemy2 = new Golem(pseudoMechant2);
            break;
        case "werewolf":
            newEnnemy2 = new Werewolf(pseudoMechant2);
            break;
    } 
    console.log(newEnnemy1);
    console.log(newEnnemy2);
    console.log(newEnnemy1.name +' affronte '+ newEnnemy2.name);
    let newBattleVV = new BattleSimulationVV(newEnnemy1, newEnnemy2);
    newBattleVV.laPartie();
}

document.getElementById("bouton3").onclick = lanceur3;


// let elf1= new Elf("Dobby");
// console.log(elf1);

// let nain1= new Nain("Patapouf");
// console.log(nain1);

// let dragon1= new Dragon("Fury");
// console.log(dragon1);

// let assassin1 = new Assassin("Tueur");
// console.log(assassin1);

// let werewolf1 = new Werewolf("Professeur Lupin");
// console.log(werewolf1);

// let berseker1 = new Berseker("Ragnar");
// console.log(berseker1);

// let griffin1 = new Griffin("Buck");
// console.log(griffin1);

// let golem1 = new Golem("Cailloux");
// console.log(golem1);

// let humain1= new Humain("Maeva");
// console.log(humain1);

// let humain2= new Humain("Pauline");
// console.log(humain2);


// let battle4 = new Battle(elf1, assassin1 );
// battle4.laPartie();

// let battle1= new Battle(humain2,griffin1);
// battle1.laPartie();

// let battle2 = new Battle(nain1, golem1);
// battle2.laPartie();

// let battle3 = new Battle(elf1,dragon1);
// battle3.laPartie();

// let battle5 = new Battle(humain2,werewolf1);
// battle5.laPartie();

// let battle6 = new Battle(nain1, berseker1);
// battle6.laPartie();

// let battleS = new BattleSimulationUD(nain1,berseker1);
// battleS.laPartie();

// let battleV = new BattleSimulationVV(dragon1,griffin1);
// battleV.laPartie();

/* random 20% de chance :
let grigri = Math.floor(Math.random(1,100)*100);
console.log(grigri);
if (grigri<=20){
    console.log('la veine !');
}else{
    console.log('pas de veine');
} */