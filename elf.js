import {Hero} from "./hero.js";

export class Elf extends Hero {

    // Ses attributs :
    race="Elf";

    // Son constructor ;
    constructor(name){
        super(name);
   }

   // Ses méthodes :

    //retourne sa force d'attaque avec
   //+10% de hitStrength sup sur les ennemis volants, -10% de hitStrength sur les ennemis terrestres
   attack(adversaire){
       if(adversaire.statutEnVol){
           console.log(this.name +" dit : \"" + adversaire.name + " est volant, super !\"");
           return this.hitStrength*1.1;
       }else{
           console.log(this.name +" dit : \"" + adversaire.name + " est terrestre, crotte !\"");
           return this.hitStrength*0.9;
       }
  }
}