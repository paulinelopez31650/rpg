export class Battle{

    // Ses attributs :
    statutPartie;
    combattantHero;
    combattantVilain;
    gagnant;
    perdant;
    healthVilain;
    compteurRound = 0;

    // Son constructor qui appelle les participants :
    constructor(combattantHero, combattantVilain){
        this.combattantHero = combattantHero;
        this.combattantVilain= combattantVilain;
    }

    // Ses méthodes :

    //Lancer le combat :
    laPartie(){

        // partie initialisée, désormais en cours
        console.log('Un nouveau combat commence, '+this.combattantHero.name+' affronte '+this.combattantVilain.name+' :');
        this.statutPartie = true;
        //On stocke la vie du Vilain pour si jamais c'est le héros qui gagne :
        this.healthVilain = this.combattantVilain.getHealth();

        //tant que les combattants sont en forme, on lance des tours de jeu :
        while (this.combattantHero.health > 0 && this.combattantVilain.health > 0){
            
            this.tourDeJeu();
        } 
        
        if(this.combattantVilain.health === 0){
            // Si le vilain est mort, il est perdant + la partie est terminée !
            this.gagnant=this.combattantHero;
            this.perdant=this.combattantVilain;
            this.statutPartie=false;
        }else if(this.combattantHero.health === 0){
            // Si le héro est mort, c'est le perdant + la partie est terminée !
            this.gagnant=this.combattantVilain;
            this.perdant=this.combattantHero;
            this.statutPartie=false;
        }
        
        // si la partie est terminée, alors on appelle la méthode finDePartie()
        if(!this.statutPartie){
            this.finDePartie();
        }
     
    }

    tourDeJeu(){
        // Qui frappe en premier ? Faire un random entre combattantHero et combattantVilain
        let lesCombattants=[this.combattantHero.name, this.combattantVilain.name]; 
        let quiCommence= Math.floor(Math.random()*2);

        // Si le random l'a choisi, c'est le heros qui frappe en premier !
        if(lesCombattants[quiCommence]===this.combattantHero.name){
            
            // Mettre la puissance d'attaque dans la variable frappeHéro et lancer l'attaque sur le Vilain
            let frappeHero=this.combattantHero.attack(this.combattantVilain);
            console.log(this.combattantHero.name + (" attaque ") + this.combattantVilain.name + (" avec une puissance de ") + frappeHero );
            //Appliquer les dégats sur les PV du Vilain
            this.combattantVilain.setHealth(-frappeHero);
            this.combattantVilain.getHealth();
         }

        // Sinon c'est le méchant qui frappe !
        else{

            //on compte les attaques du vilain pour si c'est un assassin
            this.compteurRound +=1;
            //console.log(this.compteurRound);
            //si c'est un assassin, on stocke le nbr d'attaque dans son attribut compteTour :
            if (this.combattantVilain.compteTour){
                this.combattantVilain.compteurTour=this.compteurRound;
            }
             // Mettre la puissance d'attaque dans la variable frappeVilain et lancer l'attaque sur le Heros
             let frappeVilain= this.combattantVilain.attack(this.combattantHero);
             //console.log(frappeVilain);
             console.log(this.combattantVilain.name + (" attaque ") + this.combattantHero.name + (" avec une puissance de ") + frappeVilain );
             //Appliquer les dégats sur les PV du Héros
             this.combattantHero.setHealth(-frappeVilain);
             this.combattantHero.getHealth();
        }        


    }

    finDePartie(){

        //partie finie :
        console.log("La partie est terminée !");
        console.log(this.gagnant.name + " a gagné !");

        //compter les points
        // Ajouter 3XP au gagnant :
        this.gagnant.setXp(3);
        this.gagnant.getXp();
        // Ajouter 1XP au perdant "bravo d'avoir participé":
        this.perdant.setXp(1);
        this.perdant.getXp();
        this.gagnant.getHealth();
        //console.log(this.healthVilain);
        //s'il est le gagnant, le héros regagne 10 % de la vie qu'il a pris au vilain
        if (!this.perdant.kindness){
            let gagnePV= this.healthVilain*0.1;
            //console.log(gagnePV);
            this.gagnant.setHealth(gagnePV);
            this.gagnant.getHealth();
        }
    }
}

// Une classe Battle permettant à un Heros de combattre un ennemi

/*  - un début et une fin de bataille : OK
    - rentrer en paramétre : un héros et un vilain : OK
    - créer un tour de jeu,
    - finaliser le tour de jeu: statistiques tout ça la...
    - comptage des xp OK
*/
