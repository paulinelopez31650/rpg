import {Enemy} from './enemy.js';

export class Assassin extends Enemy {
   
    // Ses attributs :
    race = "Assassin";
    compteTour=true;
    compteurTour=0;

    // Son constructor :
    constructor (name){
        super (name);
    }

    // Ses méthodes :

    attack(){
        let attaque= this.forceDuCoup(this.compteurTour);
        return attaque;
    }

    //la puissance du coup de l'assassin est plus forte à chaque attaque
    forceDuCoup(tour){
        
        let pourcentage= Math.pow(1.1, tour);
        let force = this.hitStrength * pourcentage;
        return Math.round(force);
    }
}