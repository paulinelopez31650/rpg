import {Enemy} from './enemy.js';

export class Berseker extends Enemy {
   
    // Ses attributs :
    race = "Berseker";

    // Son constructor :
    constructor (name){
        super (name);
    }

    // Ses méthodes :

    // Evolution PV  bonus de resistance de 30%
    setHealth(ajoutPV){
        if (ajoutPV<0){
            this.health += ajoutPV*0.7;
        }else{
            this.health += ajoutPV;
        }
        //tester si le personnage est encore en vie !
        if(this.health<=0){
        this.die();
        }
        return ('Les points de vie de ' + this.name + ' sont désormais de ')+ this.health + ('.');
    }
   
}