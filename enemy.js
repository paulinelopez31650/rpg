import {Personnage} from './personnage.js';

export class Enemy extends Personnage {
   
    // Ses attributs :
    kindness = false;

    // Son constructor :
    constructor (name){
        super (name);
    }

    // Ses méthodes :
    
    // Evolution PV
    setHealth(ajoutPV){

    this.health += ajoutPV;
    
    //tester si le personnage est encore en vie !
    if(this.health<=0){
    this.die();
    }
    return ('Les points de vie de ' + this.name + ' sont désormais de ')+ this.health + ('.');
  }
}

