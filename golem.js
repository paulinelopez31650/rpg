import {Enemy} from './enemy.js';

export class Golem extends Enemy {
   
    // Ses attributs :
    race = "Golem";
    
    // Son constructor :
    constructor (name){
        super (name);
    }

    // Ses méthodes :

    // Evolution PV 50% de chance d'annuler les degats recus
    setHealth(ajoutPV){
    // random pour le pourcentage de chance d'etre veinard :
    let grigri = Math.floor(Math.random(1,100)*100);

    if (ajoutPV<0 && grigri<=50){
        console.log(this.name +' n\'a même pas mal !');
    }else{
        this.health +=ajoutPV;
    }
    //tester si le personnage est encore en vie !
    if(this.health<=0){
    this.die();
    }
    return ('Les points de vie de ' + this.name + ' sont désormais de ')+ this.health + ('.');
}
}