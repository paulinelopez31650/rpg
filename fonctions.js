export var choixGentil;
export var choixMechant;
export var choixGentil1;
export var choixMechant1;
export var choixMechant2;


// Fonction pour changer les images des gentils et remplir la variable choixGentil
export function choisirGentil(elm){
    
    switch(elm.target.value){

        case "humain":
            document.getElementById("gentil").src="https://images-ext-2.discordapp.net/external/p7u2VeSM6nJ7C685nEVWrFrxRYtvCTlBTMCiImDF5jo/https/cdn.pixabay.com/photo/2018/01/21/06/07/warrior-3095911_960_720.png?width=283&height=458";
            choixGentil = "humain";
            break;

        case "nain":
            document.getElementById("gentil").src="https://images-ext-1.discordapp.net/external/H7vlCh66P_dpEYT3f4Fim1wWp5BEA-WuFQvT5zSIkD8/https/cdn.pixabay.com/photo/2019/12/24/08/44/valentine-gnome-4716233_960_720.png?width=222&height=458";
            choixGentil = "nain";
            break;

        case "elf":
            document.getElementById("gentil").src="https://images-ext-1.discordapp.net/external/lzUWwnZnWFqxZjGiAizSlFJ_p4An6cIefwdkdKQ0YzE/https/cdn.pixabay.com/photo/2020/01/01/07/55/male-elf-4733106_960_720.png?width=688&height=458";
            choixGentil = "elf";
            break;

        case "base":
            document.getElementById("gentil").src="https://images-ext-2.discordapp.net/external/SyhQennN9bNW2Se12ZoavmWcFrqT5cdeOhkY7BdwKts/https/cdn.pixabay.com/photo/2017/01/07/09/15/woman-1959984_960_720.png?width=343&height=458";
            choixGentil = "humain";
            break;
        }
    }
    export function choisirGentil2(elm){
        switch(elm.target.value){
            case "humain":
                document.getElementById("gentil1").src="https://images-ext-2.discordapp.net/external/p7u2VeSM6nJ7C685nEVWrFrxRYtvCTlBTMCiImDF5jo/https/cdn.pixabay.com/photo/2018/01/21/06/07/warrior-3095911_960_720.png?width=283&height=458";
                choixGentil1 = "humain";
                break;
    
            case "nain":
                document.getElementById("gentil1").src="https://images-ext-1.discordapp.net/external/H7vlCh66P_dpEYT3f4Fim1wWp5BEA-WuFQvT5zSIkD8/https/cdn.pixabay.com/photo/2019/12/24/08/44/valentine-gnome-4716233_960_720.png?width=222&height=458";
                choixGentil1 = "nain";
                break;
    
            case "elf":
                document.getElementById("gentil1").src="https://images-ext-1.discordapp.net/external/lzUWwnZnWFqxZjGiAizSlFJ_p4An6cIefwdkdKQ0YzE/https/cdn.pixabay.com/photo/2020/01/01/07/55/male-elf-4733106_960_720.png?width=688&height=458";
                choixGentil1 = "elf";
                break;
            }
        }
    

// Fonction pour changer les images des méchants et remplir la cariable choixMechant
export function choisirMechant(elm){

    switch(elm.target.value){
        case "dragon":
            document.getElementById("mechant").src="https://cdn.pixabay.com/photo/2019/10/09/23/41/dragon-4538390_960_720.png";
            choixMechant = "dragon";
            break;

        case "griffin":
            document.getElementById("mechant").src=" https://cdn.pixabay.com/photo/2019/02/25/00/54/griffin-4018762_960_720.png";
            choixMechant = "griffin";
            break;

        case "golem":
            document.getElementById("mechant").src="https://cdn.pixabay.com/photo/2017/05/30/21/01/figure-2358179_960_720.png";
            choixMechant = "golem";
            break;

        case "assassin":
            document.getElementById("mechant").src="https://images-ext-1.discordapp.net/external/DXoHOM1VuOeBj02e1olVUxEE-ymGI1fM_mGsemKZgu0/https/cdn.pixabay.com/photo/2020/10/08/00/33/man-5636527_960_720.png?width=458&height=458";
            choixMechant = "assassin";
            break;

        case "berseker":
            document.getElementById("mechant").src="https://cdn.pixabay.com/photo/2020/11/22/22/30/male-5768192_960_720.png";
            choixMechant = "berseker";
            break;

        case "werewolf":
            document.getElementById("mechant").src="https://images-ext-2.discordapp.net/external/4dXxEdAKnbmrE7xN4jRAcwe1isyCJBubeHlU7XjDevU/https/cdn.pixabay.com/photo/2016/08/03/14/08/werewolf-1566753_960_720.png?width=259&height=458";
            choixMechant = "werewolf";
            break;

        }
    }

export function choisirMechant1(elm){

switch(elm.target.value){
    case "dragon":
        document.getElementById("mechant1").src="https://cdn.pixabay.com/photo/2019/10/09/23/41/dragon-4538390_960_720.png";
        choixMechant1 = "dragon";
        break;

    case "griffin":
        document.getElementById("mechant1").src=" https://cdn.pixabay.com/photo/2019/02/25/00/54/griffin-4018762_960_720.png";
        choixMechant1 = "griffin";
        break;

    case "golem":
        document.getElementById("mechant1").src="https://cdn.pixabay.com/photo/2017/05/30/21/01/figure-2358179_960_720.png";
        choixMechant1 = "golem";
        break;

    case "assassin":
        document.getElementById("mechant1").src="https://images-ext-1.discordapp.net/external/DXoHOM1VuOeBj02e1olVUxEE-ymGI1fM_mGsemKZgu0/https/cdn.pixabay.com/photo/2020/10/08/00/33/man-5636527_960_720.png?width=458&height=458";
        choixMechant1 = "assassin";
        break;

    case "berseker":
        document.getElementById("mechant1").src="https://cdn.pixabay.com/photo/2020/11/22/22/30/male-5768192_960_720.png";
        choixMechant1 = "berseker";
        break;

    case "werewolf":
        document.getElementById("mechant1").src="https://images-ext-2.discordapp.net/external/4dXxEdAKnbmrE7xN4jRAcwe1isyCJBubeHlU7XjDevU/https/cdn.pixabay.com/photo/2016/08/03/14/08/werewolf-1566753_960_720.png?width=259&height=458";
        choixMechant1 = "werewolf";
        break;

    case "base":
        document.getElementById("mechant1").src="https://cdn.pixabay.com/photo/2020/08/16/01/14/fantasy-5491612_960_720.png";
        break;
    }
}

export function choisirMechant2(elm){

    switch(elm.target.value){
        case "dragon":
            document.getElementById("mechant2").src="https://cdn.pixabay.com/photo/2019/10/09/23/41/dragon-4538390_960_720.png";
            choixMechant2 = "dragon";
            break;

        case "griffin":
            document.getElementById("mechant2").src=" https://cdn.pixabay.com/photo/2019/02/25/00/54/griffin-4018762_960_720.png";
            choixMechant2 = "griffin";
            break;

        case "golem":
            document.getElementById("mechant2").src="https://cdn.pixabay.com/photo/2017/05/30/21/01/figure-2358179_960_720.png";
            choixMechant2 = "golem";
            break;

        case "assassin":
            document.getElementById("mechant2").src="https://images-ext-1.discordapp.net/external/DXoHOM1VuOeBj02e1olVUxEE-ymGI1fM_mGsemKZgu0/https/cdn.pixabay.com/photo/2020/10/08/00/33/man-5636527_960_720.png?width=458&height=458";
            choixMechant2 = "assassin";
        break;

        case "berseker":
            document.getElementById("mechant2").src="https://cdn.pixabay.com/photo/2020/11/22/22/30/male-5768192_960_720.png";
            choixMechant2 = "berseker";
        break;

        case "werewolf":
            document.getElementById("mechant2").src="https://images-ext-2.discordapp.net/external/4dXxEdAKnbmrE7xN4jRAcwe1isyCJBubeHlU7XjDevU/https/cdn.pixabay.com/photo/2016/08/03/14/08/werewolf-1566753_960_720.png?width=259&height=458";
            choixMechant2 = "werewolf";
            break;

        case "base":
            document.getElementById("mechant2").src="https://cdn.pixabay.com/photo/2020/08/16/01/14/fantasy-5491612_960_720.png";
            break;
        }
    }