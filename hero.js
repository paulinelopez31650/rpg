import {Personnage} from "./personnage.js";

export class Hero extends Personnage {
   
    // Ses attributs :
    kindness = true;
    race = "non précisée";

    // Son constructor :
    constructor(name){
        super(name);
   }

   // Ses méthodes :
    //changer sa race
   setRace(saRace){
    this.race= saRace;
    return ('Sa race est désormais... ')+ this.race +(' !');
   }

   //consulter sa race
   getRace(){
    return ('Sa race... ')+ this.race +(' !');
   }
}

//chaque combat gagné rapporte 2xp, quand l'xp atteint 10 le lvl incremente de 1
//chaque combat gagné, on recupere 10% de l'health de l'ennemi